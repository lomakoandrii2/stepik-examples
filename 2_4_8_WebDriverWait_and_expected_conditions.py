import math
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

link = 'http://suninjuly.github.io/explicit_wait2.html'

driver = webdriver.Chrome()
driver.get(link)

button = driver.find_element(By.ID, 'book')

WebDriverWait(driver, 25).until(EC.text_to_be_present_in_element((By.ID, "price"), '$100'))

button.click()
x = driver.find_element(By.ID, 'input_value').text
x = str(math.log(abs(12 * math.sin(int(x)))))
driver.find_element(By.ID, 'answer').send_keys(x)
driver.find_element(By.ID, 'solve').click()
time.sleep(5)