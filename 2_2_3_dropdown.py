from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
import time

link = 'http://suninjuly.github.io/selects1.html'

driver = webdriver.Chrome()
driver.get(link)

x = driver.find_element(By.ID, 'num1').text
y = driver.find_element(By.ID, 'num2').text
z = int(x) + int(y)

select = Select(driver.find_element(By.ID, "dropdown"))
select.select_by_value(str(z))

driver.find_element(By.TAG_NAME, 'button').click()

time.sleep(5)

driver.quit()

