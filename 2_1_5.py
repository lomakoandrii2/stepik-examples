import time

from selenium import webdriver
from selenium.webdriver.common.by import By
import math

link = 'https://suninjuly.github.io/math.html'

driver = webdriver.Chrome()
driver.get(link)

x = driver.find_element(By.ID, 'input_value').text
x = str(math.log(abs(12 * math.sin(int(x)))))


driver.find_element(By.ID, 'answer').send_keys(x)
driver.find_element(By.ID, 'robotCheckbox').click()
driver.find_element(By.ID, 'robotsRule').click()
driver.find_element(By.TAG_NAME, 'button').click()
time.sleep(5)
driver.quit()
