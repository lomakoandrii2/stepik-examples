# Также можно проскроллить всю страницу целиком на строго заданное количество пикселей. Эта команда проскроллит страницу на 100 пикселей вниз:
#browser.execute_script("window.scrollBy(0, 100);")


from selenium import webdriver
from selenium.webdriver.common.by import By
import math
import time


link = 'http://suninjuly.github.io/execute_script.html'

driver = webdriver.Chrome()
driver.get(link)

x = driver.find_element(By.ID, 'input_value').text
x = str(math.log(abs(12 * math.sin(int(x)))))
driver.find_element(By.ID, 'answer').send_keys(x)
driver.find_element(By.ID, 'robotCheckbox').click()


radio = driver.find_element(By.ID, 'robotsRule')
driver.execute_script("return arguments[0].scrollIntoView(true);", radio)
radio.click()

button = driver.find_element(By.TAG_NAME, "button")
driver.execute_script("return arguments[0].scrollIntoView(true);", button)
button.click()

time.sleep(3)


