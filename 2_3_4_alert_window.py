from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import math

link = 'http://suninjuly.github.io/alert_accept.html'

driver = webdriver.Chrome()
driver.get(link)

driver.find_element(By.TAG_NAME, 'button').click()

confirm = driver.switch_to.alert
confirm.accept()

x = driver.find_element(By.ID, 'input_value').text
x = str(math.log(abs(12 * math.sin(int(x)))))
driver.find_element(By.ID, 'answer').send_keys(x)
driver.find_element(By.TAG_NAME, 'button').click()
time.sleep(5)
driver.quit()