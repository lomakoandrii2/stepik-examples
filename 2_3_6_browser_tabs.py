import math
from selenium import webdriver
from selenium.webdriver.common.by import By
from time import sleep

link = 'http://suninjuly.github.io/redirect_accept.html'

driver = webdriver.Chrome()
driver.get(link)

driver.find_element(By.TAG_NAME, 'button').click()

tab_1 = driver.window_handles[0]  # запомнили первую вкладку
tab_2 = driver.window_handles[1]  # запомнили вторую вкладку
driver.switch_to.window(tab_2)  # выбрали нужную вкладку

x = driver.find_element(By.ID, 'input_value').text
x = str(math.log(abs(12 * math.sin(int(x)))))
driver.find_element(By.ID, 'answer').send_keys(x)
driver.find_element(By.TAG_NAME, 'button').click()
sleep(5)
driver.quit()
