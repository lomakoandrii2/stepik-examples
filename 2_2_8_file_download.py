import os
from selenium import webdriver
from selenium.webdriver.common.by import By
import time

link = 'http://suninjuly.github.io/file_input.html'

driver = webdriver.Chrome()
driver.get(link)

driver.find_element(By.NAME, 'firstname').send_keys('test')
driver.find_element(By.NAME, 'lastname').send_keys('test')
driver.find_element(By.NAME, 'email').send_keys('test')


current_dir = os.path.abspath(os.path.dirname(__file__))
file_name = "2_2_8_file_for_download.txt"
file_path = os.path.join(current_dir, file_name)
driver.find_element(By.ID, 'file').send_keys(file_path)

driver.find_element(By.TAG_NAME, 'button').click()

time.sleep(4)

driver.quit()