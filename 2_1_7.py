from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import math

link = 'http://suninjuly.github.io/get_attribute.html'

driver = webdriver.Chrome()
driver.get(link)

y = driver.find_element(By.ID, 'treasure')
x = y.get_attribute('valuex')
x = str(math.log(abs(12 * math.sin(int(x)))))
driver.find_element(By.ID, 'answer').send_keys(x)
driver.find_element(By.ID, 'robotCheckbox').click()
driver.find_element(By.ID, 'robotsRule').click()
driver.find_element(By.TAG_NAME, 'button').click()
time.sleep(5)
driver.quit()


